from django.urls import path
from .views import (
    salespeople,
    salesperson,
    customers,
    customer,
    sale,
    sales,

)

urlpatterns = [
    path("salespeople/", salespeople, name="list_salespeople"),
    path("salespeople/<int:pk>/", salesperson, name="salesperson"),
    path("customers/", customers, name="customers"),
    path("customers/<int:pk>/", customer, name="customer"),
    path("sales/", sales, name="sales"),
    path("sales/<int:pk>/", sale, name="sale"),

]
