from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import AutomobileVO, Salesperson, Customer, Sale

# Create your views here.


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "sold"]


class SalespersonDetailEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]


class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "address",
        "phone_number",
        "id",
    ]


class SaleListEncoder(ModelEncoder):
    model = Sale
    properties = ["price"]

    def get_extra_data(self, o):
        return {
            "automobile": o.automobile.vin,
            "customer_id": o.customer.id,
            "salesperson_id": o.salesperson.id,
        }


class SaleDetailEncoder(ModelEncoder):
    model = Sale
    properties = [
        "price",
        "id",
        "automobile",
        "salesperson",
        "customer",
    ]

    encoders = {
        "automobile": AutomobileVODetailEncoder(),
        "salesperson": SalespersonDetailEncoder(),
        "customer": CustomerDetailEncoder(),
    }



@require_http_methods(["GET", "POST"])
def salespeople(request):
    if request.method == "GET":
        people = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": people},
            encoder=SalespersonDetailEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            person = Salesperson.objects.create(**content)
            return JsonResponse(
                person,
                encoder=SalespersonDetailEncoder,
                safe=False
            )
        except:
            response = JsonResponse(
                {"message": "Could not create a new salesperson"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE"])
def salesperson(request, pk):
    if request.method == "DELETE":
        try:
            person = Salesperson.objects.get(id=pk)
            person.delete()
            return JsonResponse(
                person,
                encoder=SalespersonDetailEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Salesperson does not exist"})


@require_http_methods(["GET", "POST"])
def customers(request):
    if request.method == "GET":
        people = Customer.objects.all()
        return JsonResponse(
            {"customers": people},
            encoder=CustomerDetailEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            person = Customer.objects.create(**content)
            return JsonResponse(
                person,
                encoder=CustomerDetailEncoder,
                safe=False
            )
        except Exception as e:
            print(str(e))
            response = JsonResponse(
                {"message": "Could not create a new customer"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE"])
def customer(request, pk):
    if request.method == "DELETE":
        try:
            person = Customer.objects.get(id=pk)
            person.delete()
            return JsonResponse(
                person,
                encoder=CustomerDetailEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Customer does not exist"})


@require_http_methods(["GET", "POST"])
def sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleDetailEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer id"},
                status=400,
            )

        try:
            salesperson = Salesperson.objects.get(id=content["salesperson"])
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid salesperson id"},
                status=400,
            )

        try:
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile vin"},
                status=400,
            )

        sale = Sale.objects.create(**content)

        return JsonResponse(
            sale,
            encoder=SaleListEncoder,
            safe=False
        )

@require_http_methods(["DELETE"])
def sale(request, pk):
    if request.method == "DELETE":
        try:
            sale = Sale.objects.get(id=pk)
            sale.delete()
            return JsonResponse(
                sale,
                encoder=SaleDetailEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse({"message": "Sale does not exist"})
