# CarCar

Team:

* Valerie - Sales microservice
* Person 2 - Which microservice?

## Design

## Service microservice

Explain your models and integration with the inventory
microservice, here.

## Sales microservice

For my sales microservice, I created 4 models. An AutomobileVO, a Customer, a Salesperson, and a Sale models. AutomobileVO contians fields for automobile VIN number and a "sold" field with a boolean value set to be false by default. AutomobileVO recieves up-to-date Automobile data every 60 seconds from the inventory microservice through the polling process implemented in the poller.py file. Both the Customer and the Salesperson models don't contain any foreign keys. The Sale model, on the other hand, has foreign keys to all the other models for a customer, salesperson, and automobile feilds.
