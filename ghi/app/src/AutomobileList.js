import React, { useEffect, useState } from 'react';

async function fetchAutos() {
    const response = await fetch("http://localhost:8100/api/automobiles/")

    if (response.ok) {
        return response.json()
    } else {
        throw new Error("Failed to fetch automobiles")
    }
}

function AutomobileList () {
    const [autos, setAutos] = useState([])

    useEffect(() => {
        fetchAutos()
        .then(data => setAutos(data.autos))
        .catch(error => console.log(error))
    }, [])

    return (
        <>
        <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Color</th>
            <th>Year</th>
            <th>Model</th>
            <th>Manufacturer</th>
            <th>Sold</th>
          </tr>
        </thead>
        <tbody>
          {autos.map(auto => {
            const soldStatus = auto.sold ? "Yes" : "No";
            return (
            <tr key={auto.id}>
              <td>{ auto.vin }</td>
              <td>{ auto.color }</td>
              <td>{ auto.year }</td>
              <td>{ auto.model.name }</td>
              <td>{ auto.model.manufacturer.name }</td>
              <td>{ soldStatus }</td>
            </tr>
          )
          })}
        </tbody>
      </table>

        </>
    )
}

export default AutomobileList
