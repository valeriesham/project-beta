import React, { useEffect, useState } from 'react';


function AutomobileForm () {
    const [model, setModel] = useState([])
    const [formData, setFormData] = useState({
        color: '',
        year: '',
        vin: '',
        model: '',
    })

    const fetchData = async () =>{
        const url = 'http://localhost:8100/api/models/'
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json()
            setModel(data.models)
        } else {
            throw new Error ("Failed to fetch auto models")
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault()

        const url = 'http://localhost:8100/api/automobiles/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify({
                color: formData.color,
                year: formData.year,
                vin: formData.vin,
                model: formData.model,
            }),
            headers: {
                "Content-Type": "application/json",
            }
        }

        console.log(fetchConfig)
        const response = await fetch(url, fetchConfig)
        console.log(response)
        if (response.ok) {
            setFormData({
                color: '',
                year: '',
                vin: '',
                model: '',
            })
        } else {
            throw new Error ("Failed to create a new automobile")
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        })
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add an automobile to inventory</h1>
                    <form onSubmit={handleSubmit} id="create-shoes-form">
                        <div className="form-floating mb-3">
                            <input value={formData.color} onChange={handleFormChange} placeholder="Color" name="color" required type="text" id="color" className="form-control" />
                            <label htmlFor="color">Color...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.year} onChange={handleFormChange} placeholder="Year" name="year" required type="number" id="year" className="form-control" />
                            <label htmlFor="year">Year...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.vin} onChange={handleFormChange} placeholder="VIN" name="vin" required type="text" id="vin" className="form-control" />
                            <label htmlFor="vin">VIN...</label>
                        </div>
                        <div className="mb-3">
                            <select value={formData.model} onChange={handleFormChange} required name="model" id="model" className="form-select">
                                <option value="">Choose a model</option>
                                {model.map(model => {
                                    return (
                                    <option key={model.id} value={model.id}>{model.name}</option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default AutomobileForm
