import React, { useEffect, useState } from 'react';

async function fetchSalespeople() {
    const response = await fetch("http://localhost:8090/api/salespeople/")

    if (response.ok) {
        return response.json()
    } else {
        throw new Error("Failed to fetch salespeople")
    }
}

function SalesPeopleList() {
    const [people, setPeople] = useState([])

    useEffect(() => {
        fetchSalespeople()
        .then(data => setPeople(data.salespeople))
        .catch(error => console.log(error))
    })

    return (
        <>
        <table className="table table-striped">
        <thead>
          <tr>
            <th>Employee ID</th>
            <th>First Name</th>
            <th>Last Name</th>
          </tr>
        </thead>
        <tbody>
          {people.map(person => {
            return (
            <tr key={person.id}>
              <td>{ person.employee_id }</td>
              <td>{ person.first_name }</td>
              <td>{ person.last_name }</td>
            </tr>
          )
          })}
        </tbody>
      </table>

        </>
    )
}

export default SalesPeopleList
