import React, { useEffect, useState } from 'react';


function SalesHistory () {
  const [sales, setSales] = useState([]);
  const [salespeople, setSalespeople] = useState([]);
  const [filterValue, setFilterValue] = useState("")

  async function fetchSalespeople () {
    try {
    const response = await fetch('http://localhost:8090/api/salespeople/')

    if (response.ok) {
      const data = await response.json()
      setSalespeople(data.salespeople)
    } else {
      console.error("Response Invalid")
    }
  }catch (error) {
    console.error("Failed to fetch salespeople")
  }
  }

  async function fetchSales() {
    try {
    const response = await fetch('http://localhost:8090/api/sales/')

    if (response.ok) {
        const data = await response.json();
        setSales(data.sales)
    } else {
        throw new Error("Failed to fetch sales")
    }
  } catch (error) {
    console.error("Reponse Invalid")
  }
}

  useEffect(() => {
      fetchSales();
      fetchSalespeople();
  }, [])

  const handleFilterChange = (e) => {
    setFilterValue(e.target.value)
  }


  const getFilteredSales = () => {
    return sales.filter((sale) =>
      String(sale.salesperson.id)
        .includes(filterValue)
      );
  };

    return (
        <>
        <h1 style={{paddingTop: 20}}>Salesperson history</h1>
        <div className="row">
          <select className="form-select" onChange={handleFilterChange} placeholder='Salesperson'>
            <option value="">Salesperson</option>
            {salespeople.map((salesperson) => {
              return (
              <option key={salesperson.id} value={salesperson.id}>
                {salesperson.first_name} {salesperson.last_name}
              </option>
            )
          })}
        </select>
        </div>

        <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {getFilteredSales().map(sale => {
            return (
            <tr key={sale.id}>
              <td>{ sale.salesperson.first_name } { sale.salesperson.last_name }</td>
              <td>{ sale.customer.first_name } { sale.customer.last_name }</td>
              <td>{ sale.automobile.vin }</td>
              <td>{ sale.price }</td>
            </tr>
          )
          })}
        </tbody>
      </table>

        </>
    )
};

export default SalesHistory;
