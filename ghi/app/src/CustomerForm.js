import React, { useEffect, useState } from 'react';

function CustomerForm() {
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        phone_number: '',
        address: '',
    })

    const handleSubmit = async (event) => {
        event.preventDefault()
        const url = 'http://localhost:8090/api/customers/'
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json",
            }
        }

        const response = await fetch(url, fetchConfig)
        if (response.ok){
            setFormData({
                first_name: '',
                last_name: '',
                address: '',
                phone_number: '',
            })
        } else {
            throw new Error ("Failed to create a new customer")
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        })
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Add a customer</h1>
                <form onSubmit={handleSubmit} id="create-shoes-form">
                    <div className="form-floating mb-3">
                        <input value={formData.first_name} onChange={handleFormChange} placeholder="First Name" name="first_name" required type="text" id="first_name" className="form-control" />
                        <label htmlFor="first_name">First name...</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={formData.last_name} onChange={handleFormChange} placeholder="Last Name" name="last_name" required type="text" id="last_name" className="form-control" />
                        <label htmlFor="last_name">Last Name...</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={formData.address} onChange={handleFormChange} placeholder="Address" name="address" required type="text" id="address" className="form-control" />
                        <label htmlFor="employe_id">Address...</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={formData.phone_number} onChange={handleFormChange} placeholder="Phone Number" name="phone_number" required type="text" id="phone_number" className="form-control" />
                        <label htmlFor="phone_number">Phone Number...</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
    )
}

export default CustomerForm
