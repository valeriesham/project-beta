import { NavLink } from 'react-router-dom';


function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav flex-wrap me-auto mb-2 mb-lg-0">
            <li>
              <NavLink className="nav-link active text-nowrap" to='/manufacturers'>Manufacturers</NavLink>
            </li>
            <li>
              <NavLink className="nav-link active text-nowrap" to='/manufacturers/new'>Add a manufacturer</NavLink>
            </li>
            <li>
              <NavLink className="nav-link active text-nowrap" to='/models'>Models</NavLink>
            </li>
            <li>
              <NavLink className="nav-link active text-nowrap" to='/models/new'>Add a model</NavLink>
            </li>
            <li>
              <NavLink className="nav-link active text-nowrap" to='/automobiles'>Automobiles</NavLink>
            </li>
            <li>
              <NavLink className="nav-link active text-nowrap" to='/automobiles/new'>Add an automobile</NavLink>
            </li>
            <li>
              <NavLink className="nav-link active text-nowrap" to='/salespeople'>Salespeople</NavLink>
            </li>
            <li>
              <NavLink className="nav-link active text-nowrap" to='/salespeople/new'>Add a salesperson</NavLink>
            </li>
            <li>
              <NavLink className="nav-link active text-nowrap" to='/customers'>Customers</NavLink>
            </li>
            <li>
              <NavLink className="nav-link active text-nowrap" to='/customers/new'>Add a customer</NavLink>
            </li>
            <li>
              <NavLink className="nav-link active text-nowrap" to='/sales'>Sales</NavLink>
            </li>
            <li>
              <NavLink className="nav-link active text-nowrap" to='/sales/new'>Add a sale</NavLink>
            </li>
            <li>
              <NavLink className="nav-link active text-nowrap" to='/sales/history'>Sales History</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
