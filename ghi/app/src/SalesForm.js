import React, { useEffect, useState } from 'react';

function SalesForm(){
    const [autos, setAutos] = useState([])
    const [salespeople, setSalespeople] = useState([])
    const [customers, setCustomers] = useState([])
    const [formData, setFormData] = useState({
        price: '',
        automobile: '',
        salesperson: '',
        customer: '',
    });

    const fetchAutos = async () => {
        const autosResponse = await fetch('http://localhost:8100/api/automobiles/')
        if (autosResponse.ok) {
            const autosData = await autosResponse.json();
            const unsoldAutos = autosData.autos.filter(auto => !auto.sold)
            setAutos(unsoldAutos);
        } else {
            throw new Error ("Failed to fetch automobiles")
        }
    }

    const fetchSalesPeople = async () => {
        const salespeopleResponse = await fetch('http://localhost:8090/api/salespeople/');
        if (salespeopleResponse.ok) {
            const salespeopleData = await salespeopleResponse.json();
            setSalespeople(salespeopleData.salespeople)
        } else {
            throw new Error ("Failed to fetch salespeople")
        }
    }

    const fetchCustomers = async () => {
        const customersResponse = await fetch('http://localhost:8090/api/customers/');
        if (customersResponse.ok) {
            const customersData = await customersResponse.json();
            setCustomers(customersData.customers);
        } else {
            throw new Error ("Failed to fetch customers")
        }
    }

    useEffect(() => {
        fetchAutos();
        fetchSalesPeople();
        fetchCustomers();
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault();

        const salesUrl = 'http://localhost:8090/api/sales/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json",
            }
        }

        const response = await fetch(salesUrl, fetchConfig)
        if (response.ok) {
            setFormData({
                price: '',
                automobile: '',
                salesperson: '',
                customer: '',
            })
            const updateCarUrl = `http://localhost:8100/api/automobiles/${formData.automobile}/`;
            const updateCarConfig = {
                method: 'put',
                body: JSON.stringify({ sold: true }),
                headers: {
                    'Content-Type': 'application/json',
                },
            }
            await fetch(updateCarUrl, updateCarConfig)

        } else {
            throw new Error ("falied to create a new sale")
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        })
    }


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Record a new sale</h1>
                    <form onSubmit={handleSubmit} id="create-sale-form">
                        <div className="mb-3">
                        <label htmlFor="automobile">Automobile VIN</label>
                            <select onChange={handleFormChange} required name="automobile" id="automobile" className="form-select" value={formData.automobile}>
                                <option value="">Choose an automobile VIN...</option>
                                {autos.map(automobile => {
                                    return (
                                        <option key={automobile.id} value={automobile.vin}>{automobile.vin}</option>
                                        )
                                    })}
                            </select>
                        </div>
                        <div className="mb-3">
                        <label htmlFor="salesperson">Salesperson</label>
                            <select onChange={handleFormChange} required name="salesperson" id="salesperson" className="form-select" value={formData.salesperson}>
                                <option value="">Choose a salesperson...</option>
                                {salespeople.map(salesperson => {
                                    return (
                                        <option key={salesperson.id} value={salesperson.id}>{salesperson.first_name} {salesperson.last_name}</option>
                                        )
                                    })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="customer">Customer</label>
                            <select onChange={handleFormChange} required name="customer" id="customer" className="form-select" value={formData.customer}>
                                <option value="">Choose a customer...</option>
                                {customers.map(customer => {
                                    return (
                                        <option key={customer.id} value={customer.id}>{customer.first_name} {customer.last_name}</option>
                                        )
                                    })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="price">Price</label>
                            <input onChange={handleFormChange} placeholder="$" required type="number" name="price" id="price" className="form-control" value={formData.price}/>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
export default SalesForm;
