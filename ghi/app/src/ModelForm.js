import React, { useEffect, useState } from 'react';


function ModelForm () {
    const [manufacturers, setManufacturer] = useState([])
    const [formData, setFormData] = useState({
        name: '',
        picture_url: '',
    });

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json();
            setManufacturer(data.manufacturers);
        } else {
            throw new Error ("Failed to fetch manufacturer");
        }
    }

    useEffect(() => {
        fetchData()
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault()

        const url = `http://localhost:8100/api/models/`
        const fetchConfig = {
            method: "post",
            body: JSON.stringify({
                name: formData.name,
                picture_url: formData.picture_url,
                manufacturer_id: formData.manufacturer,
            }),
            headers: {
                "Content-Type": "application/json",
            }
        }

        const response  = await fetch(url, fetchConfig)
        if (response.ok) {
            setFormData({
                name: '',
                picture_url: '',
                manufacturer: '',
            })
        } else {
            throw new Error ("Failed to create new model")
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value,
        })
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Add a model</h1>
                <form onSubmit={handleSubmit} id="create-models-form">
                    <div className="form-floating mb-3">
                        <input value={formData.name} onChange={handleFormChange} placeholder="Model Name" name="name" required type="text" id="name" className="form-control" />
                        <label htmlFor="model_name">Model name...</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={formData.picture_url} onChange={handleFormChange} placeholder="Picture URL" name="picture_url" required type="text" id="picture_url" className="form-control" />
                        <label htmlFor="manufacturer">Picture URL...</label>
                    </div>
                    <div className="mb-3">
                        <select value={formData.manufacturer} onChange={handleFormChange} required name="manufacturer" id="manufacturer" className="form-select">
                            <option value="">Choose a manufacturer...</option>
                            {manufacturers.map(manufacturer => {
                                return (
                                <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                                )
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
    )
}

export default ModelForm
