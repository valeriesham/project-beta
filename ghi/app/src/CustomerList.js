import React, { useEffect, useState } from 'react';

async function fetchCustomers() {
    const response = await fetch("http://localhost:8090/api/customers/")

    if (response.ok) {
        return response.json()
    } else {
        throw new Error("Failed to fetch customers")
    }
}

function CustomerList() {
    const [people, setPeople] = useState([])

    useEffect(() => {
        fetchCustomers()
        .then(data => setPeople(data.customers))
        .catch(error => console.log(error))
    })

    return (
        <>
        <table className="table table-striped">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Phone Number</th>
            <th>Address</th>
          </tr>
        </thead>
        <tbody>
          {people.map(person => {
            return (
            <tr key={person.id}>
              <td>{ person.first_name }</td>
              <td>{ person.last_name }</td>
              <td>{ person.phone_number }</td>
              <td>{ person.address }</td>
            </tr>
          )
          })}
        </tbody>
      </table>

        </>
    )
}

export default CustomerList
