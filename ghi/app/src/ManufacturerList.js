import React, { useEffect, useState } from 'react';


async function fetchManufacturers() {
    const response = await fetch('http://localhost:8100/api/manufacturers/');

    if (response.ok) {
        return response.json();
    } else {
        throw new Error("Failed to fetch manufacturers");
    };
};


function ManufacturerList () {
    const [manufacturers, setManufacturers] = useState([]);

    useEffect(() => {
        fetchManufacturers()
            .then(data => setManufacturers(data.manufacturers))
            .catch(error => console.log(error))
    }, [])

    return (
        <>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Manufacturers</th>
            </tr>
          </thead>
          <tbody>
            {manufacturers.map(manufacturer => {
              return (
              <tr key={manufacturer.href}>
                <td>{ manufacturer.name }</td>
              </tr>
            )
            })}
          </tbody>
        </table>

      </>
    )
}

export default ManufacturerList
