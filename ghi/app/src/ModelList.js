import React, { useEffect, useState } from 'react';

async function fetchModels() {
    const response = await fetch('http://localhost:8100/api/models/');

    if (response.ok) {
        return response.json();
    } else {
        throw new Error("Failed to fetch models");
    };
};


function ModelList () {
    const [models, setModels] = useState([])

    useEffect(() => {
        fetchModels()
            .then(data => setModels(data.models))
            .catch(error => console.log(error))
    }, [])
return (
    <>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Manufacturer</th>
          <th>Picture</th>
        </tr>
      </thead>
      <tbody>
        {models.map(model => {
          return (
          <tr key={model.href}>
            <td>{ model.name }</td>
            <td>{ model.manufacturer.name }</td>
            <td><img src={ model.picture_url }
                    className="img-fluid"
                    style={{width: '200px'}}
                    alt='Model' /></td>
          </tr>
        )
        })}
      </tbody>
    </table>

  </>
)
}

export default ModelList
